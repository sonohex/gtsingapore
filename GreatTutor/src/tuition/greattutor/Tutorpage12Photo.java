package tuition.greattutor;

import java.io.ByteArrayOutputStream;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

public class Tutorpage12Photo extends Activity implements OnClickListener,
		OnFocusChangeListener {

	ImageView ivProfilePic;
	Button bUploadProfilePic;
	Button bTakePhoto;
	Bitmap bitmap, bitmap2;

	Button bUpdateProfileAndNext;
	// TextView whitehole1, whitehole2, whitehole3, whitehole4;
	RelativeLayout topbanner;
	RelativeLayout entireblackhole;
	// RelativeLayout blackhole1, paintskyblack;
	int pagenumber = 1;

	// final Animation animScale = AnimationUtils.loadAnimation(this,
	// R.animator.enlarge);

	// Button bProfileTutorial;
	// private Animation mAnimation;
	// private View mAnimationTarget;
	//
	//
	// private View mAnimationTarget;

	// View decorView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tutorpage12);

		topbanner = (RelativeLayout) findViewById(R.id.topbanner);
		entireblackhole = (RelativeLayout) findViewById(R.id.entireblackhole);
		// blackhole1 = (RelativeLayout) findViewById(R.id.blackhole1);
		// paintskyblack = (RelativeLayout) findViewById(R.id.paintskyblack);
		//
		// bProfileTutorial= (Button) findViewById(R.id.bProfileTutorial);
		// mAnimation = AnimationUtils.loadAnimation(this, R.animator.enlarge);
		// mAnimationTarget =findViewById(R.id.bProfileTutorial);
		//
		//

		topbanner.setOnClickListener(this);
		entireblackhole.setOnClickListener(this);

		// getWindow().getDecorView().setSystemUiVisibility(
		// View.SYSTEM_UI_FLAG_LAYOUT_STABLE
		// | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
		// | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
		// | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
		// | View.SYSTEM_UI_FLAG_FULLSCREEN);
		// | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

		bUpdateProfileAndNext = (Button) findViewById(R.id.bUpdateProfileAndNext);
		bUpdateProfileAndNext.setOnClickListener(this);
		
		bTakePhoto = (Button) findViewById(R.id.bTakePhoto);
		bTakePhoto.setOnClickListener(this);
		
		ivProfilePic = (ImageView)findViewById(R.id.ivProfilePic);

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.topbanner:
			if (entireblackhole.getVisibility() == View.VISIBLE) {
				entireblackhole.setVisibility(View.GONE);
			}
			// paintskyblack.setVisibility(View.VISIBLE);
			// bProfileTutorial.startAnimation(mAnimation);
			break;
		
		case R.id.entireblackhole:
			if (entireblackhole.getVisibility() == View.VISIBLE) {
				entireblackhole.setVisibility(View.GONE);
			}
			// paintskyblack.setVisibility(View.VISIBLE);
			// bProfileTutorial.startAnimation(mAnimation);

			break;

		case R.id.bTakePhoto:
			Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
		    startActivityForResult(intent, 333);
			break;
		}
	}

	
	   @Override
	   protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	      // TODO Auto-generated method stub
	      super.onActivityResult(requestCode, resultCode, data);
	      if(requestCode == 333){//take photo result
//		      Bitmap bp = (Bitmap) data.getExtras().get("data");//350x432
//		      int inWidth = bp.getWidth();//3328
//		      int inHeight = bp.getHeight();//1872
//		      boolean isHorizontal;
//		      
//		      if(inWidth>inHeight)
//		    	  isHorizontal = true;
//		      else
//		    	  isHorizontal = false;
//		      //0.81
//		      
//		      if(isHorizontal){
//		    	  
//		      }
//		      
//		      bp = ThumbnailUtils.extractThumbnail(bp, 350, 432); //the cropping code
//		      
//		      final int maxSize = 960;
//		      int outWidth;
//		      int outHeight;
//
//		      if(inWidth > inHeight){
//		          outWidth = maxSize;
//		          outHeight = (inHeight * maxSize) / inWidth; 
//		      } else {
//		          outHeight = maxSize;
//		          outWidth = (inWidth * maxSize) / inHeight; 
//		      }

		      //Bitmap resizedBitmap = Bitmap.createScaledBitmap(myBitmap, outWidth, outHeight, false);
		      
		      
		      //Bitmap.createScaledBitmap(bp, 120, 120, false);
		      // Decode image size
//		 	 BitmapFactory.Options o = new BitmapFactory.Options();
//		 	 o.inJustDecodeBounds = true;
//		 	        BitmapFactory.decodeFile(filePath, o);
		 	        
//		      bp = ThumbnailUtils.extractThumbnail(bp, 350, 432); //the cropping code		      
		      //
//		      ByteArrayOutputStream bos = new ByteArrayOutputStream();
//		      bp.compress(CompressFormat.JPEG, 100, bos);		   

	    	  
		      //ivProfilePic.setImageBitmap(bp);
	      }
	   }
	
	@Override
	public void onFocusChange(View v, boolean hasFocus) {
		// I dont know why will you need this unless we are doing a shake and
		// setError with a red caution pic for incorrect input

	}

	public void openGallery(int req_code){

        Intent intent = new Intent();

        intent.setType("image/*");

        intent.setAction(Intent.ACTION_GET_CONTENT);

        startActivityForResult(Intent.createChooser(intent,"Select file to upload "), req_code);

   }

}
